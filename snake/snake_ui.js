(function (root) {

  var SnakeGame = root.SnakeGame = (root.SnakeGame || {});

  var View = SnakeGame.View = function(htmlEl) {
    this.$el = $(htmlEl);
  };

  View.prototype.start = function() {
    var that = this;
    this.board = new SnakeGame.Board();

    window.setInterval(function() {
      that.step();
    }, 500);
  };

  View.prototype.step = function() {
    this.board.snake.move();
    this.$el = this.board.render();
  };


  View.prototype.handleKeyEvent = function(event) {
    var board = this.board;

    $('body').on("keydown", function(event){
      var key = event.keyCode;
      if (key == 37) {
        board.snake.turn("W");
      } else if (key == 38) {
        board.snake.turn("N");
      } else if (key == 39) {
        board.snake.turn("E");
      } else if (key == 40) {
        board.snake.turn("S");
      } else {
        return;
      };

    });

    // console.log(event);
  };



})(this);

$(document).ready(function() {
  $code = $('ul')
  var g = new SnakeGame.View($code);
  g.start();
  console.log(g.board);

});

// document.body.innerHTML = "";