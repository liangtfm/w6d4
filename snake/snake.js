(function (root) {

  var SnakeGame = root.SnakeGame = (root.SnakeGame || {});

  var Snake = SnakeGame.Snake = function() {
    // N = [0,-1];
    // E = [1, 0];
    // S = [0, 1];
    // W = [-1, 0];
    this.dir = "E";

    // array of coordinates
    this.segments = [new Coord(4, 4), new Coord(4, 5)];

  };

  Snake.prototype.move = function() {
    var snake = this;
    console.log("I'm moving: " + snake.dir)
    console.log(snake.segments)

    $(snake.segments).each(function(i, segment) {
      segment.plus(snake.dir)
    });

  };

  Snake.prototype.turn = function(newDir) {
    this.dir = newDir;
  };

  var Coord = SnakeGame.Coord = function (x,y) {
    this.x = x;
    this.y = y;
  };

  Coord.prototype.plus = function(dir) {

    if (dir == "N") {
      this.x -= 1;
    } else if (dir == "E") {
      this.y += 1;
    } else if (dir == "S") {
      this.x += 1;
    } else if (dir == "W") {
      this.y -= 1;
    } else {
      return;
    };
  };


  var Board = SnakeGame.Board = function() {

    this.board = [];

    for (var i = 0; i < 10; i++) {
      for (var j = 0; j < 10; j++) {
        this.board.push("<li data-id='[" + i + "," + j + "]'></li>");
      }

      var row = this.board.join("");
      row = [];
    }

    this.board.forEach(function(el) {
      $('ul').append(el);
    });

    this.snake = new Snake();
  };

  Board.prototype.render = function() {
//    document.body.innerHTML = "";
//    document.write(this.board.join(""));
    var arr = [];
    var that = this;

    $('li').each(function(index, element) {
      var x1 = parseInt($($('li')[index]).attr("data-id")[1]);
      var y1 = parseInt($($('li')[index]).attr("data-id")[3]);

      $(that.snake.segments).each(function(j, el2) {
        if ((el2.x == x1) && (el2.y == y1)) {
          $(element).toggleClass("snake");
        };
      });

      arr.push(element);
    });

    $('ul').html(arr);

    // go through the snake coordinates and then print them to screen
  };


})(this);

// $(document).ready(function(){
//
// });