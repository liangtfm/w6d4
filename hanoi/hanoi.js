(function (root) {
  var Hanoi = root.Hanoi = (root.Hanoi || {});

  var Game = Hanoi.Game = function () {
    // this.towers = [[3, 2, 1], [], []];
  };

  // Game.prototype.turn = function () {
  //
  // }
  //
  // Game.prototype.isWon = function () {
  //   // move all the discs to the last tower
  //   return (this.towers[2].length == 3) || (this.towers[1].length == 3);
  // };
  //
  // Game.prototype.isValidMove = function (startTowerIdx, endTowerIdx) {
  //   var startTower = this.towers[startTowerIdx];
  //   var endTower = this.towers[endTowerIdx];
  //
  //   if (startTower.length === 0) {
  //     return false;
  //   } else if (endTower.length == 0) {
  //     return true;
  //   } else {
  //     var topStartDisc = startTower[startTower.length - 1];
  //     var topEndDisc = endTower[endTower.length - 1];
  //     return topStartDisc < topEndDisc;
  //   }
  // };
  //
  //
  // Game.prototype.run = function () {
  //   var game = this;
  //
  //   READER.question("Enter a starting tower: ",function (start) {
  //     var startTowerIdx = parseInt(start);
  //     READER.question("Enter an ending tower: ", function (end) {
  //       var endTowerIdx = parseInt(end);
  //       game.takeTurn(startTowerIdx,endTowerIdx);
  //     });
  //   });
  // };

  Game.prototype.takeTurn = function (){

    var diskToMove;
    var game = this;

    $('.pile').click(function(){
      diskToMove = $(this).children()[0];
      game.move(diskToMove);
    });
  };

  Game.prototype.move = function (diskToMove) {

    var endTower;
    var game = this;

    $('.pile').click(function(){
      endTower = $(this);

      endTowerValue = parseInt($(endTower.children()[0]).attr("value"))
      diskToMoveValue = parseInt($(diskToMove).attr("value"))

      if (diskToMove && (endTower.children().length == 0)) {

        endTower.prepend(diskToMove);
        diskToMove = undefined;
        endTower = undefined;

       } else if ( endTowerValue > diskToMoveValue ) {

          endTower.prepend(diskToMove);
          diskToMove = undefined;
          endTower = undefined;

        } else {
          alert("Invalid Move!");
        };

      if ($($(".pile")[2]).children().length == 3) {
        alert("You win!!!!!");
      };

      $('.pile').off('click');
      game.takeTurn();
    });
  };

})(this);

$(document).ready(function(){
  new Hanoi.Game().takeTurn();
});


// this.Hanoi.Game is a constructor function, so we instantiate a new object, then run it.

// var Game = new this.Hanoi.Game();
// Game.run();